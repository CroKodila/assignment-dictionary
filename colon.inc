%define err_msg1 "Первый аргумент должен быть строкой"
%define err_msg2 "Второй аргумент должен являться меткой"
%define head 0

%macro colon 2
  %ifstr %1
    %2: dq head
    db %1, 0
    %ifid %2
      %define head %2
    %else
      %error err_msg2
    %endif
  %else
    %error err_msg1
  %endif
%endmacro
